using System.Collections.Immutable;
using Car.Domain.CarAggregate;

namespace Car.Infra;

public class InMemoryCarRepository : ICarRepository
{
    private readonly List<CarEntity> _cars = [];

    public CarEntity Add(CarEntity carEntity)
    {
        List<Wheel> persistWheels =
            carEntity.Wheels.Select((w, idx) => new Wheel(_cars.Count * 4 + idx + 1, w.Position)).ToList();
        CarEntity toPersist = new(_cars.Count + 1, carEntity.Model, persistWheels, carEntity.ManufacturingDate);
        _cars.Add(toPersist);
        return toPersist;
    }

    public ICollection<CarEntity> GetAll()
    {
        return _cars.ToImmutableList();
    }
}