﻿using Car.Api;
using Car.Api.Services;

var stockService = new CarStock();

List<CarDto> carsToCreate =
[
    new CarDto(default, ModelDto.Vw, WheelDto.Default, new DateTime(2014, 1, 5)),
    new CarDto(default, ModelDto.Vw, WheelDto.Default, new DateTime(2018, 1, 21)),
    new CarDto(default, ModelDto.Vw, WheelDto.Default, new DateTime(2001, 5, 18)),
    new CarDto(default, ModelDto.Vw, WheelDto.Default, new DateTime(2021, 3, 12)),
    new CarDto(default, ModelDto.Ford, WheelDto.Default, new DateTime(1990, 4, 23)),
    new CarDto(default, ModelDto.Ford, WheelDto.Default, new DateTime(2011, 10, 1)),
    new CarDto(default, ModelDto.Ford, WheelDto.Default, new DateTime(2016, 8, 18)),
    new CarDto(default, ModelDto.Ford, WheelDto.Default, new DateTime(2020, 11, 5))
];

carsToCreate.ForEach(car => stockService.Create(car));

ICollection<CarDto> cars = stockService.GetAll();

Console.WriteLine("Marke\tJahrgang\tMax Speed");
Console.WriteLine("=================================");
foreach (CarDto car in cars)
{
    Console.WriteLine($"{car.Model.Manufacturer}\t{car.ManufacturingDate.Year}\t\t{car.Model.MaxSpeed}");
}