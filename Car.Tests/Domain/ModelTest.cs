using Car.Domain.CarAggregate;

namespace Car.Domain.Tests.Domain;

[TestFixture]
public class ModelTest
{
    [Test]
    public void same_instance_is_equal_to_itself()
    {
        var model1 = new Model("test123", 123);

        Assert.That(model1.Equals(model1), Is.True);
    }

    [Test]
    public void equals_is_true_when_all_fields_are_equal()
    {
        var model1 = new Model("test123", 123);
        var model2 = new Model("test123", 123);

        Assert.That(model1.Equals(model2), Is.True);
    }

    [Test]
    public void equals_is_false_when_at_least_one_field_is_different()
    {
        var model1 = new Model("test123", 123);
        var model2 = new Model("anotherTest", 123);

        Assert.That(model1.Equals(model2), Is.False);
    }
}