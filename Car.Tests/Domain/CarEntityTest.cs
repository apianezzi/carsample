using Car.Common;
using Car.Domain.CarAggregate;
using Car.Domain.Common;

namespace Car.Domain.Tests.Domain;

[TestFixture]
public class CarEntityTest
{
    private readonly Model _testModel = new("test", 123);

    private readonly List<Wheel> _testWheels =
    [
        new Wheel(default, Position.BackLeft),
        new Wheel(default, Position.BackRight),
        new Wheel(default, Position.FrontLeft),
        new Wheel(default, Position.FrontRight)
    ];

    [Test]
    public void ctor_should_throw_exception_when_less_than_4_wheels()
    {
        Assert.Throws<DomainException>(() =>
            new CarEntity(default, _testModel, _testWheels.Skip(1).ToList(), DateTime.MinValue));
    }

    [Test]
    public void ctor_should_throw_exception_when_more_than_4_wheels()
    {
        Assert.Throws<DomainException>(() =>
            new CarEntity(default, _testModel, _testWheels.Concat(_testWheels).ToList(), DateTime.MinValue));
    }

    [TestCase(Position.BackRight)]
    [TestCase(Position.BackLeft)]
    [TestCase(Position.FrontLeft)]
    [TestCase(Position.FrontRight)]
    public void ctor_should_throw_exception_when_wheel_is_missing(Position testPosition)
    {
        List<Wheel> listWithoutTestPosition = _testWheels
            .Where(w => w.Position != testPosition)
            .Append(_testWheels.First(w => w.Position != testPosition))
            .ToList();

        Assert.Throws<DomainException>(() =>
            new CarEntity(default, _testModel, listWithoutTestPosition, DateTime.MinValue));
    }

    [Test]
    public void ctor_should_throw_exception_when_manufacturingDate_is_in_the_future()
    {
        Assert.Throws<DomainException>(() =>
            new CarEntity(default, _testModel, _testWheels, DateTime.Today.AddDays(1)));
    }
}