using Car.Api;
using Car.Api.Services;
using Car.Domain.CarAggregate;
using NSubstitute;

namespace Car.Domain.Tests.Services;

[TestFixture]
public class CarStockTest
{
    [SetUp]
    public void SetUp()
    {
        _carStock = new CarStock(_carRepository);
    }


    private readonly ICarRepository _carRepository = Substitute.For<ICarRepository>();
    private CarStock _carStock;

    [Test]
    public void should_create_correct_car_and_return()
    {
        CarDto testCar = new(default, ModelDto.Ford, WheelDto.Default, DateTime.MinValue);
        var expectedResponse = new CarDto(123, ModelDto.Ford, WheelDto.Default, DateTime.MinValue);
        _carRepository.Add(Arg.Any<CarEntity>()).Returns(expectedResponse.ToDomainPoco());

        CarDto result = _carStock.Create(testCar);

        AssertEqual(result, expectedResponse);
    }

    [Test]
    public void should_return_all_cars()
    {
        List<CarDto> expectedResponse =
        [
            new CarDto(123, ModelDto.Ford, WheelDto.Default, DateTime.MinValue),
            new CarDto(321, ModelDto.Ford, WheelDto.Default, DateTime.MinValue)
        ];
        _carRepository.GetAll().Returns(expectedResponse.Select(c => c.ToDomainPoco()).ToList());

        ICollection<CarDto> result = _carStock.GetAll();

        foreach (CarDto car in result)
        {
            AssertEqual(car, expectedResponse.First(c => c.Id == car.Id));
        }
    }

    private void AssertEqual(CarDto expected, CarDto actual)
    {
        Assert.That(actual.Id, Is.EqualTo(expected.Id));
        Assert.That(actual.ManufacturingDate, Is.EqualTo(expected.ManufacturingDate));
        Assert.That(actual.Model, Is.EqualTo(expected.Model));
        Assert.That(actual.Wheels, Is.EqualTo(expected.Wheels));
    }
}