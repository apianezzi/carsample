namespace Car.Domain.Common;

public abstract class Entity
{
    public int Id { get; protected init; }
}