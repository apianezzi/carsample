using Car.Common;
using Car.Domain.Common;

namespace Car.Domain.CarAggregate;

// Anmerkung: Wheel ist eine Entity, weil ich annehme, dass es sich hierbei um die tatsächlichen Räder für ein bestimmtes
// Auto handeln. Z.b. deren Pneu, Pneuzustand etc beinhalten
public class Wheel : Entity
{
    public Wheel(int id, Position position)
    {
        Id = id;
        Position = position;
    }

    // Annahme: bei der "Bezeichnung" handelt es sich schlicht um die Position und nicht eine andere wichtige Information,
    // welche persistiert werden müsste in einem string oder ähnlichem.
    public Position Position { get; }
}