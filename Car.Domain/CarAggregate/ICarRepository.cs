using Car.Domain.Common;

namespace Car.Domain.CarAggregate;

public interface ICarRepository : IRepository<CarEntity>
{
    CarEntity Add(CarEntity carEntity);
    ICollection<CarEntity> GetAll();
}