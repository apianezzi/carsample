using Car.Common;
using Car.Domain.Common;

namespace Car.Domain.CarAggregate;

public class CarEntity : Entity, IAggregateRoot
{
    public CarEntity(int id, Model model, ICollection<Wheel> wheels, DateTime manufacturingDate)
    {
        if (wheels.Count != 4)
        {
            throw new DomainException("Car must have exactly 4 wheels");
        }

        if (!ContainsCorrectWheels(wheels))
        {
            throw new DomainException("Car must have all the necessary wheel designation.");
        }

        if (manufacturingDate.Date > DateTime.Today)
        {
            //Annahme: nur Autos, welche jetzt existieren, werden hier gemanagt und es gibt keine Vorregistrierung
            throw new DomainException("Car cannot be manufactured in the future");
        }

        Model = model;
        Wheels = wheels;
        Id = id;
        ManufacturingDate = manufacturingDate;
    }

    public Model Model { get; }
    public ICollection<Wheel> Wheels { get; }
    public DateTime ManufacturingDate { get; }

    private bool ContainsCorrectWheels(ICollection<Wheel> wheels)
    {
        HashSet<Position> wheelSet = wheels.Select(w => w.Position).ToHashSet();
        return wheelSet.SetEquals(new HashSet<Position>
        {
            Position.BackLeft,
            Position.BackRight,
            Position.FrontLeft,
            Position.FrontRight
        });
    }
}