using Car.Domain.Common;

namespace Car.Domain.CarAggregate;

public class Model(string manufacturer, int maxSpeed) : ValueObject
{
    public string Manufacturer { get; } = manufacturer;
    public int MaxSpeed { get; } = maxSpeed;

    protected override IEnumerable<object> GetEqualityComponents()
    {
        return new List<object> { Manufacturer, MaxSpeed };
    }
}