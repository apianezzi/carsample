using Car.Domain.CarAggregate;
using Car.Infra;

namespace Car.Api.Services;

public class CarStock
{
    private readonly ICarRepository _carRepository;

    public CarStock()
    {
        // Anmerkung: einfachheitshalber instanziiert CarStock einfach das repo in memory und
        // es wird keine Dependency Injection angewendet.
        _carRepository = new InMemoryCarRepository();
    }

    public CarStock(ICarRepository carRepository)
    {
        _carRepository = carRepository;
    }

    public CarDto Create(CarDto car)
    {
        return _carRepository.Add(car.ToDomainPoco()).ToDto();
    }

    public ICollection<CarDto> GetAll()
    {
        return _carRepository.GetAll().Select(c => c.ToDto()).ToList();
    }
}