using Car.Common;
using Car.Domain.CarAggregate;

namespace Car.Api;

public record ModelDto(string Manufacturer, int MaxSpeed)
{
    public static ModelDto Vw => new("VW", 180);
    public static ModelDto Ford => new("Ford", 250);
}

public record WheelDto(int Id, Position Position)
{
    public static List<WheelDto> Default =>
    [
        new WheelDto(default, Position.FrontLeft),
        new WheelDto(default, Position.FrontRight),
        new WheelDto(default, Position.BackRight),
        new WheelDto(default, Position.BackLeft)
    ];
}

public record CarDto(
    int Id,
    ModelDto Model,
    List<WheelDto> Wheels,
    DateTime ManufacturingDate
);

public static class DtoExtensionMappers
{
    public static ModelDto ToDto(this Model model)
    {
        return new ModelDto(model.Manufacturer, model.MaxSpeed);
    }

    public static Model ToDomainPoco(this ModelDto dto)
    {
        return new Model(dto.Manufacturer, dto.MaxSpeed);
    }

    public static WheelDto ToDto(this Wheel wheel)
    {
        return new WheelDto(wheel.Id, wheel.Position);
    }

    public static Wheel ToDomainPoco(this WheelDto dto)
    {
        return new Wheel(dto.Id, dto.Position);
    }

    public static CarDto ToDto(this CarEntity carEntity)
    {
        return new CarDto(carEntity.Id, carEntity.Model.ToDto(),
            carEntity.Wheels.Select(w => w.ToDto()).ToList(),
            carEntity.ManufacturingDate);
    }

    public static CarEntity ToDomainPoco(this CarDto dto)
    {
        return new CarEntity(dto.Id, dto.Model.ToDomainPoco(),
            dto.Wheels.Select(w => w.ToDomainPoco()).ToList(), dto.ManufacturingDate);
    }
}