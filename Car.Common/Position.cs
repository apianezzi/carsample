namespace Car.Common;

public enum Position
{
    FrontLeft,
    FrontRight,
    BackLeft,
    BackRight
}